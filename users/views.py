# coding: utf-8

from django.contrib.auth import logout, login, authenticate
from django.shortcuts import render, redirect
from django.views.generic import View, RedirectView
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class LoginView(View):
    form_class = AuthenticationForm
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('/')

        return render(request, self.template_name, {'form': form})



class LogoutView(RedirectView):
    url = '/user/login/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class CreateUserView(View):
    form_class = UserCreationForm
    template_name = 'create_user.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(username=request.POST['username'],
                                    password=request.POST['password1'])
            login(request, user)
            return redirect('/')

        return render(request, self.template_name, {'form': form})
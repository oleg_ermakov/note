from django.conf.urls import url
from users.views import LoginView, LogoutView, CreateUserView

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^create/$', CreateUserView.as_view(), name='create_user'),
]
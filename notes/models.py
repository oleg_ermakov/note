# coding: utf-8
from django.contrib.auth.models import User
from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "категория"
        verbose_name_plural = "категории"

class Note(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField(max_length=2000)
    modified = models.DateTimeField(auto_now=True, db_index=True)
    featured = models.BooleanField(default=False, db_index=True)
    category = models.ForeignKey(Category)
    user = models.ForeignKey(User)
    uuid = models.UUIDField(blank=True, null=True, editable=False)

    class Meta:
        ordering = ['-modified']
        verbose_name = "заметка"
        verbose_name_plural = "заметки"

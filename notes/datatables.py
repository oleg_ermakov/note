class TableParamsMixin(object):

    index_column = None

    @property
    def params(self):
        return self.request.GET
    @property
    def offset(self):
        return int(self.params.get('start'))

    @property
    def limit(self):
        return int(self.params.get('length'))

    @property
    def sort_field(self):
        return self.index_column[int(self.params.get('order[0][column]'))]

    @property
    def order(self):
        if self.params.get('order[0][dir]') == 'desc':
            return '-'
        return ''

    @property
    def search(self):
        text = self.params.get('search[value]')
        if isinstance(text, int):
            text = str(text)
        return text

    def search_column(self, iColumn):
        text = self.params.get("columns[{}][search][value]".format(iColumn))
        if text:
            return text
        return False

    @property
    def draw(self):
        p = self.params.get('draw')
        return self.params.get('draw')
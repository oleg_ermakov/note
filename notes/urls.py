from django.conf.urls import url

from .views import MainView, ApiNotesView, ApiNoteView

urlpatterns = [
    url(r'api/notes$', ApiNotesView.as_view(), name='notes_api'),
    url(r'note/(?P<uuid>[^/]+)$', ApiNoteView.as_view(), name='note_api'),
    url(r'$', MainView.as_view(), name='main'),
]
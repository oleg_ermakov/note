import uuid
from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import JsonResponse

from notes.datatables import TableParamsMixin
from notes.form import NoteForm
from notes.mixins import LoginRequiredMixin
from notes.utils import coerce_put_post
from .models import Note


class MainView(LoginRequiredMixin, View):
    form_class = NoteForm
    template_name = 'main.html'
    form_template_name = 'add_note.html'

    def get(self, request, *args, **kwargs):

        notes = Note.objects.filter(user_id=request.user.id)
        form = NoteForm()
        return render(request, self.template_name, {'notes':notes, 'form': form})

    def post(self, request, *args, **kwargs):

        form = NoteForm(request.POST)
        if request.POST.get('note_id'):
            try:
                note = Note.objects.get(id=int(request.POST['note_id']))
            except Note.DoesNotExist:
                pass
            else:
                form = NoteForm(request.POST, instance=note)
        if form.is_valid():
            note = form.save(commit=False)
            note.user = request.user
            note.save()
            return JsonResponse({'type': 'success'})
        return render(request, self.form_template_name, {'form': form})

    def put(self, request, *args, **kwargs):

        coerce_put_post(request)
        note_id = int(request.PUT.get('note_id'))
        form = NoteForm()
        try:
            note = Note.objects.get(id=note_id)
        except Note.DoesNotExist:
            pass
        else:
            form = NoteForm(instance=note)
        return render(request, self.form_template_name, {'form': form})

    def delete(self, request, *args, **kwargs):

        coerce_put_post(request)
        note_id = int(request.POST.get('note_id'))
        try:
            note = Note.objects.get(id=note_id, user_id=request.user.id)
        except Note.DoesNotExist:
            return JsonResponse({'type': 'error'})
        else:
            note.delete()
        return JsonResponse({'type': 'success'})



def filter_featured(value):
    if value == 'true':
        return 'featured', True
    return 'featured', False

def filter_modified(value):
    return 'modified__contains', value.replace('\-', '-')

def filter_title(value):
    return 'title__contains', value

def filter_text(value):
    return 'text__contains', value

def filter_category(value):
    return 'category__name', value

class ApiNotesView(LoginRequiredMixin, TableParamsMixin, View):

    index_column = {0: 'title', 1: 'text', 2: 'category__name', 3: 'featured', 4: 'modified'}
    column_filter = {0: filter_title, 1: filter_text, 2: filter_category, 3: filter_featured, 4: filter_modified}


    def get(self, request, *args, **kwargs):

        self.request = request
        notes = Note.objects.filter(user_id=request.user.id)
        total = notes.count()

        query_filter = []
        for column in self.index_column.keys():
            if self.search_column(column):
                query_filter.append(self.column_filter[column](self.search_column(column)))

        notes = notes.filter(**dict(query_filter))

        if self.search:
            notes = notes.filter(Q(title__icontains = self.search) | Q(text__icontains = self.search))

        data = {'draw': self.draw,
                'recordsTotal': total,
                'recordsFiltered': notes.count(),
                'data': []}

        notes = notes.order_by('{}{}'.format(self.order, self.sort_field))[self.offset:self.offset+self.limit]

        note_append = data['data'].append
        for note in notes:
            note_append({
                'id': note.id,
                'title': note.title,
                'text': note.text,
                'category': note.category.name,
                'featured': note.featured,
                'uuid': note.uuid,
                'modified': note.modified.strftime('%Y-%m-%d')
            })

        return JsonResponse(data)

    def put(self, request, *args, **kwargs):

        coerce_put_post(request)
        type_action = request.PUT.get('type')

        note_id = int(request.PUT.get('note_id'))
        try:
            note = Note.objects.get(id=note_id)
        except Note.DoesNotExist:
            return JsonResponse({'type': 'error'})
        else:
            if type_action == 'featured':
                featured = True if 'true' in request.PUT.get('featured', '') else False
                note.featured = featured
                note.save()
                return JsonResponse({'type': 'success', 'featured': featured})
            elif type_action == 'published':
                published = True if 'true' in request.PUT.get('pub', '') else False
                if published:
                    note.uuid = uuid.uuid4()
                else:
                    note.uuid = None
                note.save()
                return JsonResponse({'type': 'success'})


        return JsonResponse({})


class ApiNoteView(View):

    template_name = 'note.html'

    def get(self, request, *args, **kwargs):
        if kwargs.get('uuid'):
            try:
                note = Note.objects.get(uuid=kwargs['uuid'])
            except Note.DoesNotExist:
                pass
            else:
                return render(request, self.template_name, {'note':note})

        return redirect('/')




# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_auto_20151115_0810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='uuid',
            field=models.UUIDField(null=True, editable=False, blank=True),
        ),
    ]

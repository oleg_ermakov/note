# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('notes', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', 'verbose_name_plural': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='note',
            options={'ordering': ['-modified'], 'verbose_name': '\u0437\u0430\u043c\u0435\u0442\u043a\u0430', 'verbose_name_plural': '\u0437\u0430\u043c\u0435\u0442\u043a\u0438'},
        ),
        migrations.AddField(
            model_name='note',
            name='user',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='note',
            name='uuid',
            field=models.UUIDField(editable=False, blank=True),
        ),
    ]

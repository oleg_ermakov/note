var table;


$(function() {
    table = $('#table-notes').dataTable({
        "processing": true,
        "serverSide": true,
        //"searching": false,
        "ajax": "/api/notes",
        "language": russian,
        "order": [[ 4, "desc" ]],
        "columns": [
            {"data": "title"},
            {"data": "text", orderable: false},
            {"data": "category"},
            {"data": "featured"},
            {"data": "modified"},
            {"data": "uuid", orderable: false},
            { "data": "id", orderable: false}
           ],
        fnInitComplete: function () {
            var select_list = [2, 3, 4];
            var search_list = [0, 1];
            this.api().columns().every( function () {
                console.log(this.index());
                var column = this;
                if (select_list.indexOf(column.index()) > -1){
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                table.fnFilter(val ? val : '', column.index(), false, false );

                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                }
                if (search_list.indexOf(column.index()) > -1){

                    var search = $('<input type="text" placeholder="Search '+$(column).text()+'" />' )
                        .appendTo( $(column.footer()).empty());
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( column.search() !== this.value ) {
                        table.fnFilter(this.value ? this.value : '', column.index(), false, false );
            }
        } );
                }
            } );
        },
        "columnDefs": [
            {
                "targets": 0,
                "data": "title",
                render: function (data, type, full, meta) {
                    if (full.uuid){
                        return '<a href="/note/' + full.uuid + '">'+ data + '</a>'
                    }
                    return data
                }}, {
                "targets": 3,
                "data": "featured",
                render: function (data, type, full, meta) {
                    var star = '';
                    if (!data){
                        star = '-empty';
                    }
                    return '<button type="button" class="star btn btn-default" data-id="'+ full.id +'" data-featured="'+ data +'">'
                                +'<span class="glyphicon glyphicon-star' + star + '" aria-hidden="true"></span>'
                        +'</button>'
                }}, {
                "targets": 5,
                "data": "uuid",
                render: function (data, type, full, meta) {
                    var glyphicon =  data ? 'ok' : 'remove';
                    var pub =  data ? true : false;
                    return '<button type="button" class="uuid btn btn-default" data-id="'+ full.id +'" data-pub="'+ pub +'">'
                                +'<span class="glyphicon glyphicon-' + glyphicon + '" aria-hidden="true"></span>'
                        +'</button>'
                }}, {
                "targets": -1,
                "data": "id",
                render: function (data, type, full, meta) {
                    return '<a href="javascript:void(0);" class="btn-xs edit" data-id='+ data +'>Изменить&nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i></a>|'
                        + '<a href="javascript:void(0);" class="btn-xs del" data-id='+ data +'>Удалить&nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i></a>';
                    }
            }]
    });
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    var csrftoken =  $('input[name="csrfmiddlewaretoken"]').val();
    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


    $('#table-notes').on('click', '.star', function() {
        var note_id = $(this).data('id');
        var featured = !$(this).data('featured');

        $.ajax({
            url: "/api/notes",
            type: "PUT",
            data: {
                type: 'featured',
                note_id: note_id,
                featured: featured
            },
            success: function(response){
                  if(response.type == "error"){
                      alert("Ошибка сервера");
                  } else{
                      var span = $('.star[data-id='+ note_id +'] span');
                      if (response.featured){
                          span.removeClass('glyphicon-star-empty');
                          span.addClass('glyphicon-star');
                      } else {
                          span.removeClass('glyphicon-star');
                          span.addClass('glyphicon-star-empty');
                      }
                  }
            },
            error: function(){
                  alert("Ошибка сервера");
            }
        });
    });

    $('#table-notes').on('click', '.uuid', function() {
        var note_id = $(this).data('id');
        var pub = !$(this).data('pub');
        $.ajax({
            url: "/api/notes",
            type: "PUT",
            data: {
                type: 'published',
                note_id: note_id,
                pub: pub
            },
            success: function(response){
                  if(response.type == "error"){
                      alert("Ошибка сервера");
                  } else{
                      table.fnDraw();
                  }
            },
            error: function(){
                  alert("Ошибка сервера");
            }
        });
    });

    $('#table-notes').on('click', '.edit', function() {
        var note_id = $(this).data('id');
        $.ajax({
            url: "/",
            type: "PUT",
            data: {
                note_id: note_id
            },
            success: function(response){
                $("#tab_add_note").html(response);
                $('#add_note input[name="note_id"]').val(note_id);
                $('a[href="#tab_add_note"]').tab('show');
            },
            error: function(){
                  alert("Ошибка сервера");
            }
        });
    });

    $('#table-notes').on('click', '.del', function() {
        var note_id = $(this).data('id');
        $.ajax({
            url: "/",
            type: "DELETE",
            data: {
                note_id: note_id
            },
            success: function(response){
                table.fnDraw();
            },
            error: function(){
                  alert("Ошибка сервера");
            }
        });
    });


    $("#tab_add_note").on('submit', 'form', function(){
        var form = $('#add_note');
        console.log(form.serialize());
        $.ajax({
            type: form.attr('method'),
            data: form.serialize(),
            success: function (data) {
                if (data.type=='success'){
                    location.reload();
                }
                $("#tab_add_note").html(data);
            },
            error: function(data) {
                alert("Ошибка сервера");
            }
        });
        return false;
    });

    $('a[href="#tab_add_note"]').click(function (e) {
        e.preventDefault();
        $(':input','#add_note')
             .not(':button, :submit, :reset, input[name="csrfmiddlewaretoken"]')
             .val('')
             .removeAttr('checked')
             .removeAttr('selected');
        $(this).tab('show')
    })
});